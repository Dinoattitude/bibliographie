<h1>Le Bluetooth
<img src="http://pngimg.com/uploads/bluetooth/bluetooth_PNG27.png" width=100>

## Description

Permettant l'échange bidirectionnel de données à courte distance en utilisant des ondes radio de 2,4 GHz. 
Son but est de simplifier les connexions entre les appareils électroniques à proximité en supprimant des liaisons filaires.

## Utilisation

Permet de remplacer les câbles entre :
- ordinateurs, 
- tablettes, 
- haut-parleurs, 
- téléphones mobiles entre eux ou avec des 
    - imprimantes, scanneurs, 
    - claviers, souris, manettes de jeu vidéo, 
    - assistants personnels, 
    - systèmes avec mains libres pour microphones ou écouteurs,
- etc...

## Limites

- La distance entre les 2 appareils est limitée à environ 10 mètres
- Consommation plus importante de la batterie pour un téléphone portable
- La transmission de données est environ de 2.1Mb/s pour le bluetooth 3.0, il faut environ 12 secondes pour transmettre une musique de 3 minutes. C'est nettement inférieur au wifi qui est de 54Mb/s environ.
- De nombreux appareils utilisent la bande radio 2,4 GHz, il est possible d'avoir des perturbations (coupures) si de nombreux objets sont connectées dans la même zone.

## Sécurité

Pour initier une connexion, la demande provient toujours d’un central. Il existe deux types d’appareils: le central (le smartphone par exemple) et le périphérique (l’objet sur lequel on souhaite se connecter, une enceinte par exemple). Lorsqu’un central essaye d’accéder à une caractéristique d’un périphérique protégé, une requête d’appairage est envoyée. L’appairage est le mécanisme de base du Bluetooth qui permet d’établir l’échange des clés entre le centrale et le périphérique ce qui donne ensuite une connexion sécurisée.

L’appairage consiste à authentifier l'identité de deux dispositifs, à chiffrer la liaison à l'aide d'une clé à court terme (STK), puis à distribuer des clés à long terme (LTK) utilisées pour le chiffrement. La LTK peut être sauvegardée pour une reconnexion plus rapide à l'avenir, c'est ce qu'on appelle le "Bonding".

## Avenir du Bluetooth

La mise en place du Bluetooth LE Audio. Véritable petite révolution, le Bluetooth LE Audio permet de faire migrer l'implantation d'un profil audio sur la partie Low Energy du Bluetooth (apparue avec la version 4.0), bien plus dynamique et plus prometteuse pour l'avenir.
En effet le Bluetooth LE Audio permet de rendre viables les solution d'aide auditive. C'était d'ailleurs la motivation première de son développement. La consommation réduite d'un produit LE Audio, que ce soit grâce à son codec optimisé ou sa couche radio un peu plus efficace, permet de standardiser un peu plus le principe.


