# Xamarin
## Le framework cross-platforme et open source de Microsoft

Qu'est-ce que Xamarin ?
.NET est une plateforme de développement composée d'outils, de langages de programmation et de bibliothèques permettant de créer de nombreux types d'applications différents.

Xamarin étend la plateforme de développement .NET avec des outils et des bibliothèques spécialement conçus pour créer des applications pour :

- Android
- iOS
- tvOS
- watchOS
- macOS 
- Windows.

# Natives et performantes

Les applications Xamarin sont des applications natives ! Que vous conceviez une interface utilisateur uniforme sur toutes les plates-formes ou que vous créiez une interface utilisateur native, vos applications se comporteront comme les utilisateurs l'attendent.

Grâce à la possibilité d'accéder à l'ensemble des fonctionnalités exposées par la plate-forme et le périphérique sous-jacents, ainsi que d'exploiter l'accélération matérielle spécifique à la plate-forme, les applications Xamarin sont compilées pour offrir des performances natives.

# Cross-platforme

Xamarin fait partie du dynamique écosystème .NET, utilisé par des millions de développeurs dans le monde. Partagez plus de 75 % de votre code sur toutes les plates-formes, pour une facilité d'utilisation de type " écrire une fois, exécuter partout ".

Utilisez vos cadres et outils préférés ainsi que les puissantes bibliothèques de Xamarin pour accéder aux API natives et aux graphiques 2D à partir du code partagé.

![Screen](https://docs.microsoft.com/fr-fr/xamarin/get-started/quickstarts/app-images/vs/notes2-ios.png)  ![screen](https://docs.microsoft.com/fr-fr/xamarin/get-started/quickstarts/app-images/vs/notes2-android.png)


# Créez de superbes interfaces utilisateur multiplateformes

Prêt à faire passer le partage de code au niveau supérieur ? Xamarin.Forms est un cadre d'interface utilisateur mobile open source de Microsoft permettant de créer des applications iOS, Android et Windows avec .NET à partir d'une base de code partagée unique.

Que vous souhaitiez une apparence cohérente sur toutes les plateformes ou que vous préfériez une apparence native, Xamarin.Forms vous permettra d'être opérationnel en un rien de temps.

# Comment utiliser Xamarin

Pour faire un projet Xamarin il faut d'abord avoir installé [Visual Studio](https://visualstudio.microsoft.com/fr/vs/preview/).
Et ensuite creer un projet Xamarin.Froms 
![Screen](https://docs.microsoft.com/fr-fr/xamarin/get-started/quickstarts/app-images/vs/new-project.png)

Les langages a connaitre sont le C# et le XAML 
Le C# est utilisé pour l'affichage mais aussi l'arrière plan, alors que le XAML est utilisa pour le balisage donc l'affichage

