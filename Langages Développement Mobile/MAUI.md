# Qu'est-ce que .NET MAUI ?
## .NET Multi-platform App UI (.NET MAUI) est un cadre multiplateforme permettant de créer des applications mobiles et de bureau natives avec C# et XAML.

> La documentation relative à l'interface utilisateur de la plate-forme multi-applications .NET (.NET MAUI) est en cours d'élaboration.

Grâce à .NET MAUI, vous pouvez développer des applications fonctionnant sur Android, iOS, macOS et Windows à partir d'une base de code unique et partagée.

![Screen](https://docs.microsoft.com/en-us/dotnet/maui/what-is-maui-images/maui.png)

.NET MAUI est une application à code source ouvert et constitue l'évolution de Xamarin.Forms, étendue des scénarios mobiles aux scénarios de bureau, avec des contrôles d'interface utilisateur reconstruits à partir de zéro pour des raisons de performance et d'extensibilité. Si vous avez déjà utilisé Xamarin.Forms pour créer des interfaces utilisateur multiplateformes, vous remarquerez de nombreuses similitudes avec .NET MAUI. Toutefois, il existe également quelques différences. Avec .NET MAUI, vous pouvez créer des applications multiplateformes à l'aide d'un seul projet, mais vous pouvez ajouter du code source et des ressources spécifiques à la plateforme si nécessaire. L'un des principaux objectifs de .NET MAUI est de vous permettre de mettre en œuvre la plus grande partie possible de la logique de votre application et de la disposition de l'interface utilisateur dans une base de code unique.

# À qui s'adresse .NET MAUI ?
.NET MAUI s'adresse aux développeurs qui souhaitent :

- Écrire des applications multiplateformes en XAML et C#, à partir d'une seule base de code partagée dans Visual Studio.
- Partager la mise en page et la conception de l'interface utilisateur sur toutes les plateformes.
- Partager le code, les tests et la logique métier entre les plateformes.

# Comment fonctionne .NET MAUI

.NET MAUI unifie les API Android, iOS, macOS et Windows en une seule API qui permet aux développeurs d'écrire une fois et de l'exécuter partout, tout en offrant un accès approfondi à tous les aspects de chaque plate-forme native.

.NET 6 fournit une série de cadres spécifiques aux plateformes pour la création d'applications : .NET pour Android, .NET pour iOS, .NET pour macOS et la bibliothèque Windows UI (WinUI). Ces cadres ont tous accès à la même bibliothèque de classes de base (BCL) de .NET 6. Cette bibliothèque permet d'abstraire de votre code les détails de la plate-forme sous-jacente. La BCL dépend du runtime .NET pour fournir l'environnement d'exécution de votre code. Pour Android, iOS et macOS, l'environnement est mis en œuvre par Mono, une implémentation du moteur d'exécution .NET. Sur Windows, WinRT joue le même rôle, mais il est optimisé pour la plate-forme Windows.

Alors que la BCL permet aux applications exécutées sur différentes plates-formes de partager une logique métier commune, les diverses plates-formes ont des méthodes différentes pour définir l'interface utilisateur d'une application, et elles fournissent des modèles différents pour spécifier comment les éléments d'une interface utilisateur communiquent et interagissent. Vous pouvez créer l'interface utilisateur pour chaque plateforme séparément en utilisant le cadre spécifique à la plateforme appropriée (.NET pour Android, .NET pour iOS, .NET pour macOS ou WinUI), mais cette approche vous oblige alors à maintenir une base de code pour chaque famille d'appareils.

.NET MAUI fournit un cadre unique pour la création d'interfaces utilisateur pour les applications mobiles et de bureau. Le diagramme suivant présente une vue de haut niveau de l'architecture d'une application .NET MAUI :

![Screen](https://docs.microsoft.com/en-us/dotnet/maui/what-is-maui-images/architecture.png)  

Dans une application .NET MAUI, vous écrivez du code qui interagit principalement avec l'API .NET MAUI (1). .NET MAUI consomme ensuite directement les API de la plate-forme native (3). En outre, le code de l'application peut exercer directement les API de la plate-forme (2), si nécessaire.

Les applications .NET MAUI peuvent être écrites sur PC ou Mac, et compilées en paquets d'applications natives :

- Les applications Android créées à l'aide de .NET MAUI sont compilées à partir de C# en langage intermédiaire (IL), qui est ensuite compilé en juste-à-temps (JIT) dans un assemblage natif au lancement de l'application.
- Les applications iOS créées à l'aide de .NET MAUI sont entièrement compilées en avance sur le temps (AOT) à partir de C# en code d'assemblage ARM natif.
- Les applications macOS créées à l'aide de .NET MAUI utilisent Mac Catalyst, une solution d'Apple qui transpose votre application iOS créée avec UIKit sur le bureau et la complète avec des API AppKit et de plate-forme supplémentaires, le cas échéant.
- Les applications Windows créées à l'aide de .NET MAUI utilisent Windows UI Library (WinUI) 3 pour créer des applications natives qui peuvent cibler le bureau Windows et la plate-forme Windows universelle (UWP). Pour plus d'informations sur WinUI, voir Windows UI Library.
 
> Pour faire des applications iOS il faut un Mac.

# Ce que fournit .NET MAUI
.NET MAUI fournit une collection de contrôles qui peuvent être utilisés pour afficher des données, lancer des actions, indiquer une activité, afficher des collections, sélectionner des données, etc. En plus d'une collection de contrôles, .NET MAUI fournit également :

- Un moteur de mise en page élaboré pour concevoir des pages.
- Plusieurs types de pages pour créer des types de navigation riches, comme des tiroirs.
- La prise en charge de la liaison de données, pour des modèles de développement plus élégants et plus faciles à maintenir.
- La possibilité de personnaliser les gestionnaires pour améliorer la présentation des éléments de l'interface utilisateur.
- Des API multiplateformes essentielles pour accéder aux fonctionnalités des appareils natifs. Ces API permettent aux - applications d'accéder à des éléments tels que le GPS, l'accéléromètre et l'état de la batterie et du réseau. Pour plus d'informations, consultez la section MAUI essentials de .NET.
- Une bibliothèque graphique multiplateforme, qui fournit une API commune pour cibler plusieurs plateformes, ce qui vous permet de partager votre code de dessin 2D entre les plateformes, ou de mélanger et d'assortir les implémentations graphiques avec une seule application.
- Un système de projet unique qui utilise le ciblage multiple pour cibler Android, iOS, macOS et Windows. Pour plus d'informations, voir .NET MAUI Single project.
- .NET hot reload, afin que vous puissiez modifier à la fois votre XAML et votre code source géré pendant l'exécution de l'application, puis observer le résultat de vos modifications sans reconstruire l'application. Pour plus d'informations, le rechargement à chaud de .NET.

# L'essentiel de .NET MAUI
.NET MAUI fournit des API multiplateformes pour les fonctionnalités natives des appareils. Voici quelques exemples de fonctionnalités fournies par .NET MAUI essentials :

- L'accès aux capteurs, tels que l'accéléromètre, la boussole et le gyroscope des appareils.
- Capacité à vérifier l'état de la connectivité réseau de l'appareil et à détecter les changements.
- Fournir des informations sur l'appareil sur lequel l'application s'exécute.
- Copier et coller du texte dans le presse-papiers du système, entre les applications.
- Prélever un ou plusieurs fichiers sur l'appareil.
- Stocker les données en toute sécurité sous forme de paires clé/valeur.
- Utiliser les moteurs de synthèse vocale intégrés pour lire le texte de l'appareil.
- Lancez des flux d'authentification basés sur le navigateur qui écoutent un rappel vers une URL spécifique enregistrée dans l'application.

# Projet unique .NET MAUI
Les applications .NET MAUI se composent généralement d'un seul projet qui peut cibler Android, iOS, macOS et Windows. Cela offre les avantages suivants :

- Un seul projet qui cible plusieurs plateformes et appareils.
- Un seul emplacement pour gérer les ressources telles que les polices et les images.
- Ciblage multiple pour organiser le code spécifique à la plateforme.

![Screen](https://docs.microsoft.com/en-us/dotnet/maui/what-is-maui-images/single-project.png)

Pour plus d'informations sur le projet unique .NET MAUI, voir [Projet unique .NET MAUI](https://docs.microsoft.com/en-us/dotnet/maui/fundamentals/single-project).

# Rechargement à chaud de .NET
.NET MAUI prend en charge le rechargement à chaud de .NET, qui vous permet de modifier votre code source géré pendant que l'application est en cours d'exécution, sans qu'il soit nécessaire de faire une pause manuelle ou d'activer un point d'arrêt. Vos modifications de code peuvent ensuite être appliquées à votre application en cours d'exécution sans recompilation.

.NET MAUI prend en charge le rechargement à chaud de XAML, ce qui vous permet d'enregistrer vos fichiers XAML et de voir les modifications reflétées dans votre application en cours d'exécution sans recompilation. En outre, l'état et les données de votre navigation seront conservés, ce qui vous permettra d'itérer rapidement sur votre interface utilisateur sans perdre votre place dans l'application.

> Pour le moment MAUI est uniquement disponible en preview, pour y accéder il faudra donc utiliser la vresion preview de Visuel Studio 2022
[Guide d'installation](https://docs.microsoft.com/en-us/dotnet/maui/get-started/installation)

