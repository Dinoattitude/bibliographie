# Applications Natives

## Définition

Une application native est une application mobile conçue spécifiquement pour un des systèmes d'exploitation utilisé par les smartphones et tablettes (iOS, Android, etc.).

## Avantages

- Le principal avantage d'une application native est qu'elle est totalement adapté au système d'exploitation visé donc elle sera plus fluide, plus rapide et aussi elle permet d'utiliser toutes les fonctionnalités liées au système d'exploitation visé (GPS,caméra, micro, etc.) et également de proposer des applications généralement plus riches que les web applications.

- Possibilité d'avoir des notifications push permettant d'alerter les utilisateurs.

- Certaines applications natives peuvent être également, une fois téléchargées et installlées localement, utilisées sans connexion Internet. (Cet avantage va être très utile aux utilisateurs quand ces derniers se situeront dans une zone sans réseau et voudront quand même profiter de leurs applications.)

## Inconvénients

- L'application native est exclusivement utilisable par les utilisateurs utilisant le système d'exploitation mobile visé.

- Le cout de développement d'une application native est relativement élevé dû à l'adaptation du développement de l'application selon la plateforme visé (différents langages de programmation selon Android ou iOS).

- Les applications natives, tout comme les logiciels bureautiques, sont construites sous forme de versions. Chaque version est égale à une application unique. Ainsi, pour effectuer des modifications structurelles sur l’app, celle-ci devra être mise à jour. Pour profiter de ces correctifs réguliers, les utilisateurs devront télécharger chaque mise à jour de leurs applications.


## Exemples d'applications natives 

Snapchat : Snapchat demande l’accès à l’appareil photo et aux contacts elle est donc forcément une application native.
Ensuite, le fait de développer en code natif permet d’aller plus loin en matière d’expérience dans l’app. On peut, par exemple, swipper à droite ou à gauche pour changer d’écran, ce qui rend la navigation très pratique. Chaque élément présent sur les pages est placé de telle sorte à rendre son accès simple et intuitif

Waze : L’application a besoin de vous géolocaliser en permanence et doit traiter énormément de données, ce qui nécessite d’exploiter la puissance du téléphone. Là est aussi l’avantage du natif. 
De plus, si vous utilisez Waze, vous avez pu admirer le travail de profondeur effectué sur l’aspect graphique de l’app. Les équipes UX design ont réalisé un travail pointu pour rendre l’application aussi esthétique et agréable à utiliser.

## Conclusion 

Les apps natives sont les applications les plus présentes et utilisées de nos jours. Quand on parle d’applications mobiles aujourd’hui, on fait référence de fait aux apps natives. Ex : Whatsapp, Messenger, Snapchat…

Le format natif a été pionnier sur mobile, et longtemps celui utilisé par défaut. Les apps natives sont de plus largement accessibles, notamment grâce à la portée des stores d’applications (Google Play, App Store) d’où elles sont téléchargeables. On en compte en 2020 plus de 6 millions disponibles sur ces plateformes.
